"""Module to classify numbers according to Nicomachus

This module offers functions to classify numbers according to Nicomachus.
Numbers can be perfect, abundant or  deficient.
"""












def classify(num):
    """Function to classify a positive integer number according to Nicomachus

    It receives a positive integer as input, finds all divisors of this number and calculates their sum (aliquot sum).
    Compare the aliquot sum to the number and:
        - if aliquot sum < number => the number is said to be deficient
        - if aliquot sum = number => the number is said to be perfect
        - if aliquot sum > number => the number is said to be abudant

    Args:
        num (int): the number to classify

    Returns:
        string: one of 'deficient', 'perfect', 'abundant'
    """
	
    try:
        num = int(num)
    except ValueError:
        return 'Please provide a positive integer.'
	
    if num < 0:
        return 'Please provide a positive integer.'

    result = 'perfect'
    s = 1
    i = 2
    while i*i < num:
        if num % i == 0:
            s += i
            s += num // i
        i += 1
    if s < num:
        result = 'deficient'
    elif s > num:
        result = 'abundant'
    return result
