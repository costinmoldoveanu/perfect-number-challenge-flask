"""Module to test the api endpoint for number classification

It is based on the Flask web microframework and pytest library.
It defines a client fixture that is used in the test functions.
Testing command is preferable to be launched in the root of the project.

Examples:
    - for brevity:
        $ pytest
    - a bit more detailed:
        $ pytest -v
    - a bit detailed and without deprecation warnings (it depends on the python/Flask version used),
    it allows to concentrate to the task at hand:
        $ pytest -v -W ignore::DeprecationWarning
"""

import os

import pytest

from web import app











@pytest.fixture
def client():
    app.config['TESTING'] = True
    client = app.test_client()

    yield client


def test_root_url(client):
    """Test root url '/'."""

    rv = client.get('/')
    assert b'Welcome' in rv.data


def test_base_api_url(client):
    """Test api url '/api'."""

    rv = client.get('/api')
    assert rv.status_code == 404 # not found


def test_api_number_deficient(client):
    """Test api url with deficient number: '/api/v0.1/classify/8'."""

    rv = client.get('/api/v0.1/classify/8')
    assert b'"classification":"deficient"' in rv.data
    assert b'"number":8' in rv.data


def test_api_number_abundant(client):
    """Test api url with abundant number: '/api/v0.1/classify/12'."""

    rv = client.get('/api/v0.1/classify/12')
    assert b'"classification":"abundant"' in rv.data
    assert b'"number":12' in rv.data


def test_api_number_perfect(client):
    """Test api url with perfect number: '/api/v0.1/classify/6'."""

    rv = client.get('/api/v0.1/classify/6')
    assert b'"classification":"perfect"' in rv.data
    assert b'"number":6' in rv.data


def test_api_alphanumeric_value(client):
    """Test api url with an alphanumeric value: '/api/v0.1/classify/6a'."""

    rv = client.get('/api/v0.1/classify/6a')
    assert rv.status_code == 404 # not found


def test_api_all_string_value(client):
    """Test api url with an all string value: '/api/v0.1/classify/abc'."""

    rv = client.get('/api/v0.1/classify/abc')
    assert rv.status_code == 404 # not found


def test_api_post_correct_request(client):
    """Test api url with post request: '/api/v0.1/classify/28'."""

    rv = client.post('/api/v0.1/classify/28')
    assert rv.status_code == 405 # not allowed


def test_api_post_wrong_request(client):
    """Test api url with post request: '/api/v0.1/classify/28a'."""

    rv = client.post('/api/v0.1/classify/28a')
    assert rv.status_code == 404 # not found
