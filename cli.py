"""Very simple/primitive utility to run 'classify' function from the command line

It receives a parameter from the command line and passes it to the 'classify' function.
No value validation is performed.

Command line example:
$ python3 cli.py 28 
"""
from sys import argv

from perfect_numbers import classify




if __name__ == '__main__':
    """Check to make sure the script is run directly and not included"""

    if len(argv) > 1:
        print( classify( argv[1] ) )
