"""Flask application to expose api endpoint for number classification

This module runs a flask based application to expose an endpoint to run number
classification according to Nicomachus.

It responds to 2 urls:
    - '/' => gives a welcome message
    - '/api/v0.1/classify/number' => where 'number' at the end is a positive integer.
    It returns a json response including the number provided and its classification.

All request are to be made with method 'GET'. Since the payload is small and doesn't
make changes on the server side (in a database usually) 

Example:
    - '/api/v0.1/classify/6' => responds with: {"classification": "perfect","number": 6}
    - '/api/v0.1/classify/8' => responds with: {"classification": "deficient","number": 8}
    - '/api/v0.1/classify/12' => responds with: {"classification": "abundant","number": 12}
"""
from flask import Flask, jsonify, request

from perfect_numbers import classify












app = Flask(__name__)

@app.route("/", methods=['GET',])
def hello():
    base_url = request.base_url
    return f"""<pre>Welcome Visitor,

Perhaps what are you looking for is at the url: <a href="{base_url}api/v0.1/classify/28">{base_url}api/v0.1/classify/28</a>
The number 28 at the end you can change to your liking to see how it is classified according to <a href="https://en.wikipedia.org/wiki/Nicomachus">Nicomachus</a>.

Other examples:
    <a href="{base_url}api/v0.1/classify/6">{base_url}api/v0.1/classify/6</a>
    <a href="{base_url}api/v0.1/classify/8">{base_url}api/v0.1/classify/8</a>
    <a href="{base_url}api/v0.1/classify/12">{base_url}api/v0.1/classify/12</a>

Have fun</pre>"""

@app.route("/api/v0.1/classify/<int:number>", methods=['GET',])
def get_classify(number):
    response = {"number": number,"classification": classify(number)}
    return jsonify(response)

